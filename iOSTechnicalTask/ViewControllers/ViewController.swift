//
//  ViewController.swift
//  iOSTechnicalTask
//
//  Created by macbook on 7/1/19.
//  Copyright © 2019 Develandoo. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var todayCollectionView: UICollectionView!
    @IBOutlet weak var next5DaysCollectionView: UICollectionView!
    
    
    let locationManager = CLLocationManager()
    let todayWeatherCollectionCellCount: Int = 8
    let next5DaysWeatherCollectionCellCount: Int = 5
    var latitude: CLLocationDegrees = 0.0
    var longitude: CLLocationDegrees = 0.0
    
    var todayForecast: NSMutableArray = NSMutableArray()
    var next5DaysForecast: NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let city = WeatherService.instance.searchedCity {
            getWeatherForCity(city:city)
        }
    }
    
    func getMylocationWeather() {
        WeatherService.instance.weatherByLocation(lat: latitude, andLon: longitude) { result in
            self.setupCurrentWeather(response: result)
        }
        WeatherService.instance.forecastByLocation(lat: latitude, andLon: longitude) { result in
            self.setupCurrentForecast(response: result)
        }
    }
    
    func getWeatherForCity(city name: String) {
        WeatherService.instance.weatherByText(search: name) { result in
            self.setupCurrentWeather(response: result)
        }
        WeatherService.instance.forecastByText(search: name) { result in
            self.setupCurrentForecast(response: result)
        }
    }
    
    func setupCurrentWeather(response weather: WeatherResponse) {
        humidityLabel.text = "Humidity: \(weather.main?.humidity ?? 0)%"
        minTempLabel.text = "Min Temp: \(Int(weather.main?.temp_min ?? 0))°"
        maxTempLabel.text = "Max temp: \(Int(weather.main?.temp_max ?? 0))°"
        pressureLabel.text = "Pressure: \(weather.main?.pressure ?? 0)"
        cityLabel.text = weather.name ?? ""
        descriptionLabel.text = weather.weather?[0].description ?? ""
        tempLabel.text = "\(Int(weather.main?.temp ?? 0))°"
    }
    
    func setupCurrentForecast(response forecast:ForecastResponse) {
        todayForecast.removeAllObjects()
        next5DaysForecast.removeAllObjects()
        for i in 0..<todayWeatherCollectionCellCount {
            if let list = forecast.list?[i] {
                todayForecast.add(list)
            }
        }
        for i in 0..<(forecast.list?.count ?? 0) {
            if let list = forecast.list?[i] {
                let date = Date(timeIntervalSince1970: list.dt ?? 0)
                let hour = Calendar.current.component(.hour, from: date)
                if hour > 13 && hour < 17 {
                    next5DaysForecast.add(list)
                }
            }
        }
        print(next5DaysForecast.count)
        todayCollectionView.reloadData()
        next5DaysCollectionView.reloadData()
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        latitude = locValue.latitude
        longitude = locValue.longitude
        
        getMylocationWeather()
        
        locationManager.stopUpdatingLocation()
    }
}

