//
//  SearchViewController.swift
//  iOSTechnicalTask
//
//  Created by macbook on 7/8/19.
//  Copyright © 2019 Develandoo. All rights reserved.
//

import UIKit

class SearchViewController: UITableViewController, UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var infoLabel: UILabel!
    
    var searchResult: NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        infoLabel.isHidden = false
        infoLabel.text = "Searching..."
        WeatherService.instance.cancelSearchRequest()
        WeatherService.instance.weatherByText(search: searchText) { (response) in
            self.searchResult.removeAllObjects()
            if let name = response.name {
                self.infoLabel.isHidden = true
                self.searchResult.add(name)
            } else {
                self.infoLabel.text = "No result found"
            }
            self.tableView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResult.count;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = searchResult[indexPath.row] as? String
        return cell;
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        WeatherService.instance.searchedCity = searchResult[indexPath.row] as? String
        self.navigationController?.popToRootViewController(animated: true)
    }
}
