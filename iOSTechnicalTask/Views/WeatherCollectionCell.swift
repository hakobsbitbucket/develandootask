//
//  WeatherCollectionCell.swift
//  iOSTechnicalTask
//
//  Created by macbook on 7/4/19.
//  Copyright © 2019 Develandoo. All rights reserved.
//

import Foundation
import UIKit

class WeatherCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var weatherIcon: UIImageView!
    
    @IBOutlet weak var weatherTime: UILabel!
    
    @IBOutlet weak var weatherTemp: UILabel!
    
}
