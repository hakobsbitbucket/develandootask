//
//  ViewControllerCollectionViewExtension.swift
//  iOSTechnicalTask
//
//  Created by macbook on 7/4/19.
//  Copyright © 2019 Develandoo. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

fileprivate let iconBaseUrl: String = "https://openweathermap.org/img/wn/";
fileprivate let iconUrlAddition: String = ".png"
fileprivate let iconUrlAdditionLarge: String = "@2x.png"

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == todayCollectionView {
            return todayForecast.count
        }
        return next5DaysForecast.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! WeatherCollectionCell
        if collectionView == todayCollectionView {
            let listItem = todayForecast[indexPath.row] as? ForecastList
            let date = Date(timeIntervalSince1970: listItem?.dt ?? 0)
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
            dateFormatter.locale = NSLocale.current
            dateFormatter.dateFormat = "HH:mm"
            let strDate = dateFormatter.string(from: date)
            cell.weatherTime.text = strDate
            cell.weatherTemp.text = "\(Int(listItem?.main?.temp ?? 0))°"
            Alamofire.request(iconBaseUrl + (listItem?.weather?[0].icon ?? "") + iconUrlAdditionLarge).response { response in
                if let data = response.data {
                    let image = UIImage(data: data)
                    cell.weatherIcon.image = image
                } else {
                    print("Data is nil. I don't know what to do :(")
                }
            }
        } else {
            let listItem = next5DaysForecast[indexPath.row] as? ForecastList
            let date = Date(timeIntervalSince1970: listItem?.dt ?? 0)
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
            dateFormatter.locale = NSLocale.current
            dateFormatter.dateFormat = "MM/dd"
            let strDate = dateFormatter.string(from: date)
            cell.weatherTime.text = strDate
            cell.weatherTemp.text = "\(Int(listItem?.main?.temp ?? 0))°"
            Alamofire.request(iconBaseUrl + (listItem?.weather?[0].icon ?? "") + iconUrlAdditionLarge).response { response in
                if let data = response.data {
                    let image = UIImage(data: data)
                    cell.weatherIcon.image = image
                } else {
                    print("Data is nil. I don't know what to do :(")
                }
            }
        }
        return cell;
    }
}
