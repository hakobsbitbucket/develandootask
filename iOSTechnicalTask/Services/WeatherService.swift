//
//  RequestManager.swift
//  iOSTechnicalTask
//
//  Created by macbook on 7/1/19.
//  Copyright © 2019 Develandoo. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

fileprivate let baseUrl:String = "https://api.openweathermap.org/data/2.5/";
fileprivate let urlGlobalParameters:String = "units=metric&APPID=bd15ccbe82356c7fd80b849765c95ef7";

class WeatherService {
    
    static let instance = WeatherService()
    
    private init() {}
    
    var searchRequest: DataRequest?
    var searchedCity: String?
    
    func weatherByLocation(lat latitude:Double, andLon longitude:Double, completion: @escaping (_ result: WeatherResponse)->()) -> Void {
        let url = baseUrl + "weather?lat=\(latitude)&lon=\(longitude)&\(urlGlobalParameters)";
        print(url);
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .validate { request, response, data in
                // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                return .success
            }
            .responseJSON { response in
                print(response)
            }
            .responseObject { (response: DataResponse<WeatherResponse>) in
                print(response);
                switch response.result {
                case .success(let value):
                    let weatherResponse = value
                    completion(weatherResponse)
                case .failure(let error):
                    print(error)
                }
        }
    }
    
    func forecastByLocation(lat latitude:Double, andLon longitude:Double, completion: @escaping (_ result: ForecastResponse)->()) -> Void {
        let url = baseUrl + "forecast?lat=\(latitude)&lon=\(longitude)&\(urlGlobalParameters)";
        print(url);
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .validate { request, response, data in
                // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                return .success
            }
            .responseJSON { response in
                print(response)
            }
            .responseObject { (response: DataResponse<ForecastResponse>) in
                print(response);
                switch response.result {
                case .success(let value):
                    let weatherResponse = value
                    completion(weatherResponse)
                case .failure(let error):
                    print(error)
                }
        }
    }
    
    func weatherByText(search text:String, completion: @escaping (_ result: WeatherResponse)->()) -> Void {
        let url = baseUrl + "weather?q=\(text)&\(urlGlobalParameters)";
        print(url);
        searchRequest = Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .validate { request, response, data in
                // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                return .success
            }
            .responseJSON { response in
                print(response)
            }
            .responseObject { (response: DataResponse<WeatherResponse>) in
                print(response);
                switch response.result {
                case .success(let value):
                    let weatherResponse = value
                    completion(weatherResponse)
                case .failure(let error):
                    print(error)
                }
        }
    }
    
    func forecastByText(search text:String, completion: @escaping (_ result: ForecastResponse)->()) -> Void {
        let url = baseUrl + "forecast?q=\(text)&\(urlGlobalParameters)";
        print(url);
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .validate { request, response, data in
                // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                return .success
            }
            .responseJSON { response in
                print(response)
            }
            .responseObject { (response: DataResponse<ForecastResponse>) in
                print(response);
                switch response.result {
                case .success(let value):
                    let weatherResponse = value
                    completion(weatherResponse)
                case .failure(let error):
                    print(error)
                }
        }
    }
    
    func cancelSearchRequest() -> Void {
        searchRequest?.cancel()
    }
}
