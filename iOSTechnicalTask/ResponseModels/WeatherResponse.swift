//
//  WeatherResponse.swift
//  iOSTechnicalTask
//
//  Created by macbook on 7/2/19.
//  Copyright © 2019 Develandoo. All rights reserved.
//

import Foundation
import ObjectMapper

class WeatherResponse: BaseResponse {
    var name: String?
    var main: Main?
    var weather: Array<Weather>?
    
    required init?(map: Map){
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        name <- map["name"]
        main <- map["main"]
        weather <- map["weather"]
    }
}

class Main: Mappable {
    var humidity: Float?
    var pressure: Float?
    var temp: Float?
    var temp_min: Float?
    var temp_max: Float?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        humidity <- map["humidity"]
        pressure <- map["pressure"]
        temp <- map["temp"]
        temp_min <- map["temp_min"]
        temp_max <- map["temp_max"]
    }
}

class Weather: Mappable {
    var description: String?
    var icon: String?
    var main: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        description <- map["description"]
        icon <- map["icon"]
        main <- map["temp"]
    }
}
