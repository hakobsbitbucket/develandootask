//
//  BaseResponse.swift
//  iOSTechnicalTask
//
//  Created by macbook on 7/3/19.
//  Copyright © 2019 Develandoo. All rights reserved.
//

import Foundation
import ObjectMapper

class BaseResponse: Mappable {
    var cod: Int?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        cod <- map["cod"]
    }
}
