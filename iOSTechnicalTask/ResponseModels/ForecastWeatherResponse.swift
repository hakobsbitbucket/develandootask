//
//  ForecastWeatherResponse.swift
//  iOSTechnicalTask
//
//  Created by macbook on 7/2/19.
//  Copyright © 2019 Develandoo. All rights reserved.
//

import Foundation
import ObjectMapper

class ForecastResponse: BaseResponse {
    var city: City?
    var message: Double?
    var cnt: String?
    var list: Array<ForecastList>?
    
    required init?(map: Map){
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        city <- map["city"]
        message <- map["message"]
        cnt <- map["cnt"]
        list <- map["list"]
    }
}

class City: Mappable {
    var id: String?
    var name: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
    }
}

class ForecastList: Mappable {
    var dt: Double?
    var main: Main?
    var weather: Array<Weather>?
    var clouds: Clouds?
    var wind: Wind?
    var dt_txt: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        dt <- map["dt"]
        main <- map["main"]
        weather <- map["weather"]
        clouds <- map["clouds"]
        wind <- map["wind"]
        dt_txt <- map["dt_txt"]
    }
}

class Clouds: Mappable {
    var all: Int?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        all <- map["all"]
    }
}

class Wind: Mappable {
    var speed: String?
    var deg: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        speed <- map["speed"]
        deg <- map["deg"]
    }
}

class Temp: Mappable {
    var day: String?
    var min: String?
    var max: String?
    var night: String?
    var eve: String?
    var morn: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        day <- map["day"]
        min <- map["min"]
        max <- map["max"]
        night <- map["night"]
        eve <- map["eve"]
        morn <- map["morn"]
    }
}

//class DailyForecastList: Mappable {
//    var dt: Int?
//    var temp: Temp?
//    var weather: Array<Weather>?
//    var pressure: Float?
//    var humidity: Int?
//
//    required init?(map: Map){
//
//    }
//
//    func mapping(map: Map) {
//        dt <- map["id"]
//        temp <- map["temp"]
//        weather <- map["weather"]
//        pressure <- map["pressure"]
//        humidity <- map["humidity"]
//    }
//}

//class DailyForcastResponse: Mappable {
//    var city: City?
//    var country: String?
//    var cod: Int?
//    var message: String?
//    var cnt: Int?
//    var list: Array<DailyForecastList>?
//
//    required init?(map: Map){
//
//    }
//
//    func mapping(map: Map) {
//        city <- map["city"]
//        country <- map["country"]
//        cod <- map["cod"]
//        message <- map["message"]
//        cnt <- map["cnt"]
//        list <- map["list"]
//    }
//}
